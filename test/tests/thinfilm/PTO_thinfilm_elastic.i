[Mesh]
  file = exodus_thinfilm_09_20_10_10.e
  #uniform_refine = 1
[]

[GlobalParams]
  disp_x = disp_x
  disp_y = disp_y
  disp_z = disp_z
  displacements = 'disp_x disp_y disp_z'
  prefactor = 0.01 #negative = tension, positive = compression
[]



[Variables]
  [./disp_x]
    order = FIRST
    family = LAGRANGE
  [../]
  [./disp_y]
    order = FIRST
    family = LAGRANGE
  [../]
  [./disp_z]
    order = FIRST
    family = LAGRANGE
  [../]
[]


[Materials]
  [./eigen_strain_zz] #Use for stress-free strain (ie epitaxial)
   type = ComputeEigenstrain
   block = '1'
  # eigen_base = 'exx exy exz eyx eyy eyz ezx ezy ezz'
   eigen_base = '1 0 0 0 1 0 0 0 0'
 [../]

  [./elasticity_tensor_1]
    type = ComputeElasticityTensor
    fill_method = symmetric9
    C_ijkl = '380. 150. 150. 380. 150. 380. 110. 110. 110.'
    block = '1'
  [../]
  [./strain_1]
    type = ComputeSmallStrain
    block = '1'
  [../]
  [./stress_1]
    type = ComputeLinearElasticStress
    block = '1'
  [../]

  [./elasticity_tensor_2]
    type = ComputeElasticityTensor
    C_ijkl = '319 99.6 99.6 319 99.6 319 109.53 109.53 109.53'
    fill_method = symmetric9
    block = '2'
  [../]
  [./strain_2]
    type = ComputeSmallStrain
    block = '2'
  [../]
  [./stress_2]
    type = ComputeLinearElasticStress
    block = '2'
  [../]
[]


[Kernels]
  #Elastic problem
  [./TensorMechanics]
  #This is an action block
  [../]

[]


[BCs]
  [./bot_disp_x]
    variable = disp_x
    type = DirichletBC
    value = 0
    boundary = '7'
  [../]
  [./bot_disp_y]
    variable = disp_y
    type = DirichletBC
    value = 0
    boundary = '7'
  [../]
  [./bot_disp_z]
    variable = disp_z
    type = DirichletBC
    value = 0
    boundary = '7'
  [../]

  [./Periodic]
    [./TB_disp_x_pbc]
      variable = disp_x
      primary = '3'
      secondary = '5'
      translation = '0 20 0'
    [../]
    [./TB_disp_y_pbc]
      variable = disp_y
      primary = '3'
      secondary = '5'
      translation = '0 20 0'
    [../]
    [./TB_disp_z_pbc]
      variable = disp_z
      primary = '3'
      secondary = '5'
      translation = '0 20 0'
    [../]

    [./TBsub_disp_x_pbc]
      variable = disp_x
      primary = '8'
      secondary = '10'
      translation = '0 20 0'
    [../]
    [./TBsub_disp_y_pbc]
      variable = disp_y
      primary = '8'
      secondary = '10'
      translation = '0 20 0'
    [../]
    [./TBsub_disp_z_pbc]
      variable = disp_z
      primary = '8'
      secondary = '10'
      translation = '0 20 0'
    [../]

    [./RL_disp_x_pbc]
      variable = disp_x
      primary = '4'
      secondary = '6'
      translation = '20 0 0'
    [../]
    [./RL_disp_y_pbc]
      variable = disp_y
      primary = '4'
      secondary = '6'
      translation = '20 0 0'
    [../]
    [./RL_disp_z_pbc]
      variable = disp_z
      primary = '4'
      secondary = '6'
      translation = '20 0 0'
    [../]

  
    [./RLsub_disp_x_pbc]
      variable = disp_x
      primary = '9'
      secondary = '11'
      translation = '20 0 0'
    [../]
    [./RLsub_disp_y_pbc]
      variable = disp_y
      primary = '9'
      secondary = '11'
      translation = '20 0 0'
    [../]
    [./RLsub_disp_z_pbc]
      variable = disp_z
      primary = '9'
      secondary = '11'
      translation = '20 0 0'
    [../]
  [../]
[]



#[Postprocessors]
#    [./Felastic]
#      type = ElasticEnergy
#      block = '1'
#      execute_on = 'timestep_end'
#    [../]
#[]


[Preconditioning]
  [./smp]
    type = SMP
    full = true
    petsc_options = '-snes_view -snes_linesearch_monitor -snes_converged_reason -ksp_converged_reason -ksp_snes_ew'
    petsc_options_iname = '-ksp_gmres_restart  -snes_rtol -ksp_rtol -pc_type'
    petsc_options_value = '    121                1e-8      1e-8    bjacobi'
  [../]
[]

[Executioner]
  type = Steady
  solve_type = 'NEWTON'
[]

[Outputs]
  print_linear_residuals = true
  print_perf_log = true
  [./out]
    type = Exodus
    file_base = out_thinfilm_elastic
    elemental_as_nodal = true
  [../]
[]
